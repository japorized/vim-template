# vim-template

Vim has basic functionality for templating, but as far as I am aware, you can only have one template for each filetype. That is far too limiting, at least for my use cases.

One can certainly use plugins like Ultisnips for templating, but that is not what snippet plugins are meant for.

---

## Installation

    git clone https://gitlab.com/japorized/vim-template.git ~/.vim/pack/vim-template/start

#### Pathogen

    # if you have not installed Pathogen
    mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
    
    git clone https://gitlab.com/japorized/vim-template.git ~/.vim/bundle

#### Optional Dependency

* [Denite](https://github.com/Shougo/denite.nvim)

---

## Usage

1. Put templates in `g:templates_dir` (default: $HOME/.vim/templates/)
2. Call for `:GetTemplate` in a buffer
3. Select wanted template
4. ???
5. Profit

If you wish to use Denite, just call for

    :Denite gettemplates

For a full documentation, see `:h vim-template`.

---

## Breaking changes

* *(Mar 11, 2019)* For those using Denite, I decided to break the keyword `templates` into `gettemplates` and `edittemplates`.
