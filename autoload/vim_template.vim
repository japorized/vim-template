let s:templates_dir = get(g:, 'templates_dir', $HOME . '/.vim/templates')
let s:templates_autoft = get(g:, 'templates_autoft', '1')

function vim_template#DeniteGetTemplate(...) abort
  if isdirectory(s:templates_dir)
    if s:templates_dir =~ '[/\\]'  " if contains path separator (slash or backslash)
      let dir = fnamemodify(s:templates_dir, ':p:h')
      if s:templates_autoft && ! a:0
        let fnm = &filetype
      else
        let fnm = s:templates_dir
      endif
    else
      let dir = expand('%:p:h')  " directory of current file
      let fnm = s:templates_dir
    endif
    
    if empty(fnm)
      let fnm = '**/' . fnm
    else
      let fnm = fnm . '**'
    endif
    let s:templates = filter(split(globpath(dir, fnm), '\n'), '!isdirectory(v:val)')
    if empty(s:templates)
      let s:templates = filter(split(globpath(dir, '*'), '\n'), '!isdirectory(v:val)')
    endif
    return s:templates
  else
    call vim_template#NoTemplateDir()
  endif
endfunction

function vim_template#GetTemplate() abort
  " if the templates directory exists, then we look for the templates
  if isdirectory(s:templates_dir)
    let selection = vim_template#SelectTemplate()

    " if the template indeed exists, then we proceed with applying the template
    if filereadable(selection)
      call vim_template#ApplyTemplate(selection, bufnr('%'))
    endif
    
  " otherwise, prompt the user to create the template directory
  else
    call vim_template#NoTemplateDir()
  endif
endfunction

" apply template
function vim_template#ApplyTemplate(template, bufnr)
  if !filereadable(a:template)
    echohl WarningMsg
    echom '[vim-template] Template does not exist!'
    echohl None
  else
    " if the buffer is empty, apply the template without asking
    if line('$') == 1 && getline(1) == ''
      execute a:bufnr . 'bufdo! 1,$d|0r ' . a:template

    " otherwise, warn the user that the template will overwrite existing
    " contents
    else
      let confirm = input('This replaces existing contents. Are you sure? (Y/n) ')
      if confirm == 'Y' || confirm == 'y' || confirm == ''
        " clear the buffer and apply template
        execute a:bufnr . 'bufdo! 1,$d|0r ' . a:template
      elseif confirm == 'N' || confirm == 'n'
        echom '[vim-template] Action cancelled.'
      else
        echohl WarningMsg
        echom '[vim-template] Input not understood. Action cancelled.'
        echohl None
      endif
    endif
  endif
endfunction

function vim_template#EditTemplate(...)
  if isdirectory(s:templates_dir)
    if a:0 == 0
      let selection = vim_template#SelectTemplate(0)
    else
      let selection = a:1
    endif

    if filereadable(selection)
      execute "tabnew " selection
    endif

  else
    call vim_template#NoTemplateDir()
  endif
endfunction

" provide truthy to NOT select by filetype
" will only use filetype if option is enabled ft is defined
function vim_template#SelectTemplate(...)
  if s:templates_autoft && &filetype == '' || a:0
    " no - since it is some generic filetype
    let s:select = input("Template: ", s:templates_dir . '/', "file_in_path")
  else
    " use <filetype>- to filter out templates by filetype
    let s:select = input("Template: ", s:templates_dir . '/' . &filetype . '-', "file_in_path")
  endif
  redraw!
  let s:file = join(split(s:select, 'Template: /'))
  if s:file == ''
    echom '[vim-template] Action cancelled.'
    return 0
  endif
  return globpath('/', s:file)
endfunction

" function to prompt the user to create templates directory
function vim_template#NoTemplateDir()
  let choice = input('Directory $HOME/.vim/templates does not exist. Create one? (Y/n) ')
  if choice == 'Y' || choice == 'y' || choice == ''
    call mkdir(s:templates_dir)
    redraw!
    echom '[vim-template] ' . s:templates_dir . ' created'
  elseif choice == 'N' || choice == 'n'
    redraw!
    echom '[vim-template] ' . s:templates_dir . ' not created.'
  else
    redraw!
    echohl WarningMsg
    echo '[vim-template] Invalid argument. Directory not created.'
    echohl None
  endif
endfunction
