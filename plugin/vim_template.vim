command! -nargs=0 GetTemplate call vim_template#GetTemplate()
command! -nargs=0 EditTemplate call vim_template#EditTemplate()
