# -*- coding:utf-8 -*-
# FILE: edittemplates.py
# AUTHOR: japorized <japorized (at) tutanota (dot) io>
# License: MIT license

from .base import Base

class Source(Base):

    def __init__(self, vim):
        super().__init__(vim)
        self.name = 'edittemplates'
        self.kind = 'command'

    def gather_candidates(self, context):
        buf = self.vim.current.buffer
        return [{
            'word': x,
            'action__command': f"call vim_template#EditTemplate('{x}')"
        } for x in self.vim.eval('vim_template#DeniteGetTemplate(1)')]
